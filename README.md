## Description
A very simple wasm package for converting to and from utf-8.

This library basically only wraps rust-encoding with wasm-pack.

## Development
Requires wasm-pack for compiling, e.g. `wasm-pack build --target web`.

The decision to use rust-encoding was made because of the section "Why not ICU or rust-encoding?" in https://hsivonen.fi/encoding_rs/
> As noted above, a key requirement was the ability to decode to and from both UTF-16 and UTF-8, but ICU supports only decoding to and from UTF-16 and rust-encoding supports only decoding to and from UTF-8.

Decoding to and from UTF-8 fulfills our desired purposes.

## Hints for usage
The index.html is simply an example for usage.

https://en.wikipedia.org/wiki/Windows_code_page