extern crate encoding;

use encoding::{Encoding, EncoderTrap};
use encoding::all::WINDOWS_1252;
use encoding::all::ISO_8859_1;

use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub fn rust_encode_to_iso_8859_1(s: &str) -> Vec<u8> {
	match ISO_8859_1.encode(s, EncoderTrap::Ignore) {
		Err(why) => eprintln!("oh noes {:?}", why),
		Ok(answer) => return answer,
	}
	vec![]
}

#[wasm_bindgen]
pub fn rust_encode_to_windows_1252(s: &str) -> Vec<u8> {
	match WINDOWS_1252.encode(s, EncoderTrap::Ignore) {
		Err(why) => eprintln!("oh noes {:?}", why),
		Ok(answer) => return answer,
	}
	vec![]
}

#[cfg(test)]
mod tests {
	use super::*;
	#[test]
	fn windows_1252_works() {
		// https://en.wikipedia.org/wiki/Windows-1252
		assert_eq!(rust_encode_to_windows_1252("abc"), vec![97,98,99]);
		assert_eq!(rust_encode_to_windows_1252("äöüß"), vec![228, 246, 252, 223]);
		assert_eq!(rust_encode_to_windows_1252("\u{20AC}"), vec![128]); // €
		assert_eq!(rust_encode_to_windows_1252("漢字"), vec![]); // Kanji not part of WINDOWS_1252
	}

	#[test]
	fn iso_8859_1_works() {
		// https://en.wikipedia.org/wiki/ISO/IEC_8859-1
		assert_eq!(rust_encode_to_iso_8859_1("abc"), vec![97,98,99]);
		assert_eq!(rust_encode_to_iso_8859_1("äöüß"), vec![228, 246, 252, 223]);
		assert_eq!(rust_encode_to_iso_8859_1("\u{20AC}"), vec![]); // € not part of ISO_8859_1
		assert_eq!(rust_encode_to_iso_8859_1("漢字"), vec![]); // Kanji not part of ISO_8859_1
	}
}
